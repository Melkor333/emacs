* Todo before it works
** Copy the .gnus file to ~/

* Mailboxes setup
** Yosemitesam
   #+begin_src emacs-lisp
(setq gnus-select-method
	     '(nnimap "mail.yosemitesam.ch"
		      (nnimap-address "mail.yosemitesam.ch")
		      (nnimap-stream tls)
		      (nnimap-port 993)))
   #+end_src

** Andeo business
   #+begin_src emacs-lisp
(add-to-list 'gnus-secondary-select-methods
	     '(nnimap "andeo"
		      (nnimap-address "pim.andeo.ch")
		      (nnimap-stream tls)
		      (nnimap-port 993)))
   #+end_src

* generic settings (performance, etc.)
** Personal informations
   #+begin_src emacs-lisp
(setq user-full-name "Samuel Ruprecht"
      user-mail-address "sam@yosemitesam.ch")
   #+end_src
** Use full cache
   #+begin_src emacs-lisp
; NO 'passive -> use the full cache instead
(setq gnus-use-cache t)
   #+end_src

** Store encrypted password
   #+begin_src emacs-lisp
(setq epa-file-cache-passphrase-for-symmetric-encryption t)
   #+end_src

* Mail view settings
** keep Threads together
   #+begin_src emacs-lisp
(setq gnus-summary-thread-gathering-function 'gnus-gather-threads-by-subject)
   #+end_src
** Show read mails
   #+begin_src emacs-lisp
   (setq gnus-fetch-old-headers 'some)
   #+end_src
* Todo: use or delete the following stuff
  ;(require 'nnir)

;; Please note mail folders in `gnus-select-method' have NO prefix like "nnimap+hotmail:" or "nnimap+gmail:"
;(setq gnus-select-method nil) ;; Read feeds/atom through gwene

;; ask encryption password once

;; @see http://gnus.org/manual/gnus_397.html
;(add-to-list 'gnus-secondary-select-methods
;             '(nnimap "gmail"
;                      (nnimap-address "imap.gmail.com")
;                      (nnimap-server-port 993)
;                      (nnimap-stream ssl)
;                      (nnir-search-engine imap)
                      ; @see http://www.gnu.org/software/emacs/manual/html_node/gnus/Expiring-Mail.html
                      ;; press 'E' to expire email
;                      (nnmail-expiry-target "nnimap+gmail:[Gmail]/Trash")
;                      (nnmail-expiry-wait 90)))

;; OPTIONAL, the setup for Microsoft Hotmail
;(add-to-list 'gnus-secondary-select-methods
;             '(nnimap "hotmail"
;                      (nnimap-address "imap-mail.outlook.com")
;                      (nnimap-server-port 993)
;                      (nnimap-stream ssl)
;                      (nnir-search-engine imap)
;                      (nnmail-expiry-wait 90)))

;(add-to-list 'gnus-secondary-select-methods
;             '(nnimap "andeo.ch"
;                      (nnimap-address "pim.andeo.ch")
;                      (nnimap-stream tls)
;		      (nnimap-port 993)))



;(setq gnus-thread-sort-functions
;      '(gnus-thread-sort-by-most-recent-date
;        (not gnus-thread-sort-by-number)))


;; {{ press "o" to view all groups
;(defun my-gnus-group-list-subscribed-groups ()
;  "List all subscribed groups with or without un-read messages"
;  (interactive)
;  (gnus-group-list-all-groups 5))

;(define-key gnus-group-mode-map
;  ;; list all the subscribed groups even they contain zero un-read messages
;  (kbd "o") 'my-gnus-group-list-subscribed-groups)
; }}

;; BBDB: Address list
;(add-to-list 'load-path "/Users/samuelruprecht/.bbdb/")
;(require 'bbdb)
;(bbdb-initialize 'message 'gnus 'sendmail)
;(add-hook 'gnus-startup-hook 'bbdb-insinuate-gnus)
;(setq bbdb/mail-auto-create-p t
;      bbdb/news-auto-create-p t)

;; auto-complete emacs address using bbdb UI
;(add-hook 'message-mode-hook
;          '(lambda ()
;             (flyspell-mode t)
;             (local-set-key (kbd "TAB") 'bbdb-complete-name)))

;; Fetch only part of the article if we can.
;; I saw this in someone's .gnus
;(setq gnus-read-active-file 'some)

;; open attachment
;(eval-after-load 'mailcap
;  '(progn
;     (cond
      ;; on macOS, maybe change mailcap-mime-data?
;      ((eq system-type 'darwin))
      ;; on Windows, maybe change mailcap-mime-data?
;      ((eq system-type 'windows-nt))
;      (t
       ;; Linux, read ~/.mailcap
;       (mailcap-parse-mailcaps)))))

;; Tree view for groups.
;(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

;; Threads!  I hate reading un-threaded email -- especially mailing
;; lists.  This helps a ton!

;; Also, I prefer to see only the top level message.  If a message has
;; several replies or is part of a thread, only show the first message.
;; `gnus-thread-ignore-subject' will ignore the subject and
;; look at 'In-Reply-To:' and 'References:' headers.
;(setq gnus-thread-hide-subtree t)
;(setq gnus-thread-ignore-subject t)

;; Read HTML mail:
;; You need install the command line web browser 'w3m' and Emacs plugin 'w3m'
;; manually. It specify the html render as w3m so my setup works on all versions
;; of Emacs.
;;
;; Since Emacs 24+, a default html rendering engine `shr' is provided:
;;   - It works out of box without any cli program dependency or setup
;;   - It can render html color
;; So below line is optional.
;;(setq mm-text-html-renderer 'w3m) ; OPTIONAL

;; Send email through SMTP
;(setq message-send-mail-function 'smtpmail-send-it
;      smtpmail-default-smtp-server "smtp.yosemitesam.ch"
;      smtpmail-smtp-service 587)
;; http://www.gnu.org/software/emacs/manual/html_node/gnus/_005b9_002e2_005d.html
; ^ this link is dead.. soooooo...
;(setq gnus-use-correct-string-widths nil)

;; Sample on how to organize mail folders.
;; It's dependent on `gnus-topic-mode'.
;(eval-after-load 'gnus-topic
;  '(progn
;     (setq gnus-message-archive-group '((format-time-string "sent.%Y")))
;     (setq gnus-server-alist '(("archive" nnfolder "archive" (nnfolder-directory "~/Mail/archive")
;                                (nnfolder-active-file "~/Mail/archive/active")
;                                (nnfolder-get-new-mail nil)
;                                (nnfolder-inhibit-expiry t))))

     ;; "Gnus" is the root folder, and there are three mail accounts, "misc", "hotmail", "gmail"
;     (setq gnus-topic-topology '(("Gnus" visible)
;                                 (("misc" visible))
;                                 (("hotmail" visible nil nil))
;                                 (("gmail" visible nil nil))))

     ;; each topic corresponds to a public imap folder
;     (setq gnus-topic-alist '(("hotmail" ; the key of topic
;                               "nnimap+hotmail:Inbox"
;                               "nnimap+hotmail:Drafts"
;                               "nnimap+hotmail:Sent"
;                               "nnimap+hotmail:Junk"
;                               "nnimap+hotmail:Deleted")
;                              ("gmail" ; the key of topic
;                               "nnimap+gmail:INBOX"
;                               "nnimap+gmail:[Gmail]/Sent Mail"
;                               "nnimap+gmail:[Gmail]/Trash"
;                               "nnimap+gmail:Drafts")
;                              ("misc" ; the key of topic
;                               "nnfolder+archive:sent.2018"
;                               "nnfolder+archive:sent.2019"
;                               "nndraft:drafts")
;                              ("Gnus")))))
